

@ECHO OFF

if "%1%" == "" GOTO Nomsg

call git add .
call git commit -a -m "%1"
call git push -u origin master
:eof

:Nomsg
call git add .
call git commit -a -m "update"
call git push -u origin master

:eof