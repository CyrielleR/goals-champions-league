jQuery(document).ready(function(event){

	jQuery("#").carouFredSel({
		circular: true,
		infinite: true,
		auto 	: false,
		pauseOnHover : true,

		prev	: {
			button	: "#",
		},
		next	: {
			button	: "#",
		},
	});

	jQuery('#main-page #welcomeModal').modal();


	if (jQuery("#").children().length < 5 ) {
		chosenPlayers = chosenPlayers + 1;
		jQuery("html,body").animate({
			scrollTop : "0"
		}, 100,'swing', function(){});
		jQuery(this).addClass("chosen");
		jQuery(this).clone().appendTo("#element")
		.draggable({containment: "#container"})
		.css("position", "absolute")
		.css("top","0")
		.css("left", "0");
	}
});

jQuery("#saveImageForm").submit(function(event){

	if (chosenPlayers != 5) {
		jQuery('#errorModal').modal();
		event.preventDefault();

		jQuery('#errorModal').find('.modal-title').text('Error');
		jQuery('#errorModal').find('.modal-body').text('You Need To Choose 5 Players!');
	}else{
		jQuery("#pitch-container").css("background-image","url(img/background.jpg)");
		html2canvas(jQuery("#pitch-container"),{
			onrendered : function(canvas){
				$('#img_val').val(canvas.toDataURL("image/png"));
				document.getElementById("saveImageForm").submit();
			}
		});
	}
});
});