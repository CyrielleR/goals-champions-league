
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	<meta name="viewport" content="width=device-width">
	<link rel="stylesheet" href="css/bootstrap/bootstrap.css">
	<link rel="stylesheet" href="css/base.css">
	<link href='http://fonts.googleapis.com/css?family=Raleway:300,400,900,800,700,500,600' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="shortcut icon" href="favicon.ico" />
</head>

<body id="main-page">

	<div id="top">
		<div class="top-background"></div>
			<div class="col-sm-12">
				<div class="logo">
					<img src="img/logo.png" alt="logo Goals"/>
				</div>
				<div class="cl-logo">
					<img src="img/champions-logo.png" alt="logo Champions league"/>
				</div>
				<h1>Champions league final 2015</h1>
				<ul>
					<li><h2>Barcelona</h2></li>
					<li><img src="img/barca-logo.png" alt="logo barca"/></li>
					<li><h2>VS</h2></li>
					<li><img src="img/juventus-logo.png" alt="logo juventus"/></li>
					<li><h2>Juventus</h2></li>
				</ul>
				<h4>June 6th, 2015 | 19:45</h4>
			</div>
		</div>
	</div>

	<div id="location" class"clear-me">
		<h3>Location</h3>
		<img class="map" src="img/map.png" alt="map"/>
		<h4 class="stade">Olympic stadium, Berlin</h4>

				<img class="pad1" src="img/barca-logo.png" alt="logo barca"/>
				<img class="pad2" src="img/manager-barca.png" alt="manager barca"/>
				<h4 class="name">Luis Enrique<br/><sub>Barcelona Manager</sub></h4>

				<img class="pad3"src="img/manager-juventus.png" alt="manager juventus"/>
				<img class="pad4"src="img/juventus-logo.png" alt="manager juventus"/>
				<h4 class="name2">Massimiliano Allegri<br/><sub>Juventus Manager</sub></h4>

			</div>
		</div>
	</div>

	<div id="victory" class"clear-me">
		<h3>Biggest Victory</h3>
		<div class="row">
			<div class="col-sm-6">
				<p>4 - 0</p>
				<ul class="col-left">
					<li><h5>Barcelona</h5></li>
					<li><h5>Apoel</h5></li>
				</ul>
			</div>
			<div class="col-sm-6">
				<p>3 - 0</p>
				<ul class="col-right">
					<li><h5>Juventus</h5></li>
					<li><h5>Dortmund</h5></li>
				</ul>
			</div>
		</div>	 
	</div>

	<div id="defeat" class"clear-me">
		<h3>Biggest Defeat</h3>
			<div class="row">
				<div class="col-sm-6">
					<p>3 - 2</p>
					<ul class="col-left">
						<li><h5>Bayern Munich</h5></li>
						<li><h5>Barcelona</h5></li>
					</ul>
					<ul class="col-left">
						<li><h5>PSG</h5></li>
						<li><h5>Barcelona</h5></li>
					</ul>
				</div>
				<div class="col-sm-6">
					<p>1 - 0</p>
					<ul class="col-right">
						<li><h5>Olympiacos</h5></li>
						<li><h5>Juventus</h5></li>
					</ul>
					<ul class="col-right">
						<li><h5>Alterico Madrid</h5></li>
						<li><h5>Juventus</h5></li>
					</ul>
				</div>
			</div>	
	</div>
<div id="test">
	<div id="scorer" class"clear-me">
		<h3>Top Scorer</h3>
		<div class="row">
			<div class="col-sm-6">
				<h4 class="name">Lonel Messi</h4>
				<img class="one" src="img/player-barca.png" alt="joueur barca"/>
				<img class="line-name" src="img/barre-left.png" alt="barre"/>
				<h6 class="abso">10 goals <br/> <sub>in 12 Games</sub></h6>
			</div>
			<div class="col-sm-6">
				<h4 class="name2">Carlos Tevez</h4>
				<img class="two" src="img/player-juventus.png" alt="joueur juventus"/>
				<img class="line-name2" src="img/barre-right.png" alt="barre"/>
				<h6 class="abso2">7 goals <br/><sub>in 12 games</sub></h6>
			</div>
		</div>
	</div>

	<div id="goal">
		<h3>Goals</h3>
			<div class="row">
				<div class="col-sm-4">
					<img src="img/procent-barca.png" alt="% barca"/>
				</div>
				<div class="col-sm-4">
					<ul>
						<li><img class="little" src="img/carre-blue-foncé.png" alt="carré"/>Goals scored</li>
						<li><img class="little" src="img/carre-blue-clair.png" alt="carré"/>Goals conceded</li>
					</ul>
				</div>
				<div class="col-sm-4">
					<img src="img/procent-juventus.png" alt="% juventus"/>
				</div>
			</div>
	</div>

	<div id="passes">
		<h3>Completed Passes</h3>
			<div class="row">
				<div class="col-sm-3">
					<img class="pad" src="img/passe-left.png" alt="ball"/>
				</div>
				<div class="col-sm-3">
					<h6 class="left">7101 passes</h6>
				</div>
				<div class="col-sm-3">
					<h6 class="right">5388 passes</h6>
				</div>
				<div class="col-sm-3">
					<img src="img/passe-right.png" alt="ball"/>
				</div>
			</div>
	</div>

	<div id="booking">
		<h3>Booking</h3>
			<div class="row">
				<div class="col-sm-3">
					<p class="yellow">19</p>
					<ul>
						<li><img src="img/card-y.png" alt="yellow card"/></li>
						<li><h2>Yellow cards</h2></li>
					</ul>
				</div>
				<div class="col-sm-3">
					<p class="red">1</p>
					<ul>
						<li><img src="img/card-red.png" alt="red card"/></li>
						<li><h2>Red cards</h2></li>
					</ul>
				</div>
				<div class="col-sm-3">
					<p class="yellow">23</p>
					<ul>
						<li><img src="img/card-y.png" alt="yellow card"/></li>
						<li><h2>Yellow cards</h2></li>
					</ul>
				</div>
				<div class="col-sm-3">
					<p class="red">0</p>
					<ul>
						<li><img src="img/card-red.png" alt="red card"/></li>
						<li><h2>Red cards</h2></li>
					</ul>
				</div>
			</div>
	</div>
</div>
	<div id="bottom">
		<a>goalfootball.co.uk</a>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel1">Modal title</h4>
				</div>
				<div class="modal-body">
					...
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

</body>

<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap/modal.js"></script>
<script src="js/script.js"></script>

</html>